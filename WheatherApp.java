import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class WheatherApp {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to the Weather App!");

        // Gather 5 locations from the user
        String[] locations = new String[5];
        for (int i = 0; i < locations.length; i++) {
            System.out.print("Enter location " + (i+1) + ": ");
            locations[i] = scanner.nextLine();
        }

        // Generate an option of days from 1 to 3 days
        int days = 0;
        while (days < 1 || days > 3) {
            System.out.print("How many days ahead would you like to check? (1-3): ");
            days = scanner.nextInt();
            scanner.nextLine(); // consume the newline character
        }

        // Use the OpenWeatherMap API to get the weather information for each location
        String apiKey = "your_api_key_here";
        String url = "http://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s&units=metric";
        try {
            for (String location : locations) {
                String formattedUrl = String.format(url, location, apiKey);
                URL apiUrl = new URL(formattedUrl);
                HttpURLConnection conn = (HttpURLConnection) apiUrl.openConnection();
                conn.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // Parse the JSON response and get the temperature
                JSONObject jsonObj = new JSONObject(response.toString());
                JSONObject mainObj = jsonObj.getJSONObject("main");
                double temp = mainObj.getDouble("temp");

                // Suggest appropriate clothing based on the temperature
                String suggestion = "";
                if (temp < 5) {
                    suggestion = "Wear a heavy coat, gloves, hat, and scarf.";
                } else if (temp >= 5 && temp < 10) {
                    suggestion = "Wear a jacket or sweater.";
                } else if (temp >= 10 && temp < 20) {
                    suggestion = "Wear a light jacket or long sleeves.";
                } else if (temp >= 20 && temp < 25) {
                    suggestion = "Wear shorts or a skirt with a t-shirt or blouse.";
                } else if (temp >= 25 && temp < 30) {
                    suggestion = "Wear shorts, a tank top, or a light dress.";
                } else {
                    suggestion = "Wear light and loose clothing to stay cool.";
                }

                // Print the suggestion for the user
                System.out.println(location + " (in " + days + " days): " + suggestion);
            }
        } catch (Exception e) {
            System.out.println("An error occurred: " + e.getMessage());
        }

        System.out.println("Thank you for using the Weather App!");
    }
}
